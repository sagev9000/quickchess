import React from 'react';
import { textFromBoard } from './logic';

import Board from './components/Board';
import * as game from './components/Board';

it('detects friendship and enemyship', () => {
  const black = new game.Piece(game.BLACK, game.KING);
  const black2 = new game.Piece(game.BLACK, game.QUEEN);
  const white = new game.Piece(game.WHITE, game.KING);
  const white2 = new game.Piece(game.WHITE, game.QUEEN);
  const empty = new game.Piece(game.EMPTY, game.EMPTY);
  const empty2 = new game.Piece(game.EMPTY, game.EMPTY);

  const truths = [
    black.isFriendOf(black2),
    white.isFriendOf(white2),

    black.isEnemyOf(white),
    white.isEnemyOf(black),
  ];
  for (const truth of truths) {
    expect(truth).toBe(true);
  }

  const lies = [
    black.isFriendOf(white), black.isFriendOf(empty),
    white.isFriendOf(black), white.isFriendOf(empty),
    empty.isFriendOf(white), empty.isFriendOf(black), empty.isFriendOf(empty2),

    black.isEnemyOf(black2), black.isEnemyOf(empty),
    white.isEnemyOf(white2), white.isEnemyOf(empty),
    empty.isEnemyOf(white), empty.isEnemyOf(black), empty.isEnemyOf(empty2),
  ];
  for (const lie of lies) {
    expect(lie).toBe(false);
  }
});

it('is created correctly', () => {
  const board = new Board();
  const rows = ['B',
    'rnbqkbnr',
    'pppppppp',
    '________',
    '________',
    '________',
    '________',
    'PPPPPPPP',
    'RNBQKBNR'
  ];
  expect(textFromBoard(board)).toBe(rows.join(''));
  for (const square of board.state.squares) {
    expect(square.hasNotMoved()).toBe(true);
  }
});

it('is created from text correctly', () => {
  const rows = ['B',
    'pppppppp',
    'pppppppp',
    '________',
    '________',
    '________',
    '________',
    'PPPPPPPP',
    'PPPPPPPP'
  ];
  const board = new Board({text: rows.join('')});
  expect(textFromBoard(board)).toBe(rows.join(''));
});

it('detects an obvious checkmate', () => {
  const rows = ['B',
    'q_______',
    'q______K',
    'q_______',
    '________',
    '________',
    '________',
    '________',
    'k_______'
  ];
  const board = new Board({text: rows.join('')});
  const inCheck = board.whoInCheck();
  expect(board.checkmate()).toBe(true)
  expect(inCheck.type).toEqual(game.KING);
  expect(inCheck.color).toEqual(game.BLACK);
});

it('detects basic castling', () => {
  const startRows = ['B',
    '____k___',
    '________',
    '________',
    '________',
    '________',
    '________',
    'PPPPPPPP',
    'R___K__R'
  ];
  // const endRows = ['B',
  //   '____k___',
  //   '________',
  //   '________',
  //   '________',
  //   '________',
  //   '________',
  //   'PPPPPPPP',
  //   'R____RK_'
  // ];
  let board = new Board({text: startRows.join('')});
  let move = board.isValidMove(60, 62);
  expect(move.x).toBe(6);
  expect(move.y).toBe(7);
});
