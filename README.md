# QuickChess

This is a simple implementation of Chess in React. It supports all moves, including castling and en passant. A live build can be seen here:
[https://quickchess.win/](https://quickchess.win/). Assets
borrowed from Wikipedia.

# Building

After cloning the repo, building is as simple as running `npm i` to download
all dependencies, followed by `npm start` to run the game at
[http://localhost:3000](http://localhost:3000), or `npm build` to generate a
static version of the page.
